﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pozzani.Tickets.Models
{
    public class Note
    {
        /// <summary>
        /// Unique Identifier
        /// </summary>
        [Key]
        public Int32 ID { get; set; }

        /// <summary>
        /// Note text
        /// </summary>
        [Required]
        [AllowHtml]
        public String Description { get; set; }

        /// <summary>
        /// Time taken
        /// </summary>
        public Int32 Minutes { get; set; }

        /// <summary>
        /// Date / Time Stramp for note
        /// </summary>
        public DateTime DateSubmitted { get; set; }

        /// <summary>
        /// User that entered note
        /// </summary>
        [ForeignKey("User")]
        public Int32 SubmittedByID { get; set; }
        public virtual User User { get; set; }

        /// <summary>
        /// Ticket that note is referring to
        /// </summary>
        [ForeignKey("Ticket")]
        public Int32 TicketID { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}