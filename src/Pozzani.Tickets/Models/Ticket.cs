﻿using Pozzani.Tickets.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pozzani.Tickets.Models
{
    public class Ticket
    {
        /// <summary>
        /// Account Statuses
        /// </summary>
        public enum StatusItems
        {
            New,
            UnAssigned,
            Assigned,
            Paused,
            Awaiting_Feedback,
            Completed,
            Deleted
        }

        public enum PriorityItems
        {
            Low,
            Medium,
            High,
            Urgent,
            Critical
        }

        /// <summary>
        /// Unique Id for ticket
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Ticket description
        /// </summary>
        [Required]
        [AllowHtml]
        public string Description { get; set; }

        /// <summary>
        /// Displays a shorter version of the description limited to 253 chars
        /// </summary>
        public string ShortDescription
        {
            get
            {
                if (Description.Length > 250)
                {
                    return Description.Substring(0, 250) + "...";
                }

                return Description;
            }
        }

        /// <summary>
        /// Time stamp for when ticket was created
        /// </summary>
        [Display(Name = "Date Submitted")]
        public DateTime DateSubmitted { get; set; }

        /// <summary>
        /// Time stamp for when ticket was last updated
        /// </summary>
        [Display(Name = "Last Updated")]
        public DateTime DateLastUpdated { get; set; }

        /// <summary>
        /// Time stamp for when ticket was completed
        /// </summary>
        [Display(Name = "Date Completed")]
        public DateTime? DateCompleted { get; set; }

        /// <summary>
        /// User that the ticket is assigned to
        /// </summary>
        [ForeignKey("AssignedTo")]
        [CascadeDelete]
        [Display(Name = "Assigned To")]
        public int? AssignedToId { get; set; }

        /// <summary>
        /// User that submitted the original ticket
        /// </summary>
        [ForeignKey("SubmittedBy")]
        [CascadeDelete]
        [Display(Name = "Submitted By")]
        public int SubmittedById { get; set; }

        /// <summary>
        /// Ticket type Id
        /// </summary>
        [ForeignKey("TicketType")]
        [Display(Name = "Ticket Type")]
        public int TicketTypeId { get; set; }

        /// <summary>
        /// Current Status of Ticket
        /// </summary>
        public StatusItems Status { get; set; }

        public PriorityItems Priority { get; set; }

        /// <summary>
        /// This is for the ordering of jobs 1 being the 1st ticket to action
        /// </summary>
        [Display(Name = "Priority Level")]
        public int PriorityLevel { get; set; }

        /// <summary>
        /// Link to User class for assigned users
        /// </summary>
        public virtual User AssignedTo { get; set; }

        /// <summary>
        /// Link to User class for submitted users
        /// </summary>
        public virtual User SubmittedBy { get; set; }

        /// <summary>
        /// Link to TicketType class for ticket types
        /// </summary>
        public virtual TicketType TicketType { get; set; }

        /// <summary>
        /// Complete list of notes for this ticket
        /// </summary>
        public virtual List<Note> Notes { get; set; }
    }
}