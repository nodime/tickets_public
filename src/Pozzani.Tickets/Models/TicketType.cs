﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pozzani.Tickets.Models
{
    public class TicketType
    {
        /// <summary>
        /// Ticket Type Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Name of ticket type
        /// </summary>
        public string Name { get; set; }
    }
}