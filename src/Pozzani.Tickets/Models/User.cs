﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pozzani.Tickets.Models
{
    public class User
    {
        public enum AccessLevelList
        {
            Disabled,
            Super,
            IT,
            Management,
            Office,
            Marketing,
            Warehouse,
            Standard
        }

        /// <summary>
        /// User Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Username, this will be the same as the users Windows username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Users Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Users Email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Site Access level
        /// </summary>
        /// TODO: 
        public AccessLevelList AccessLevel { get; set; }
    }
}