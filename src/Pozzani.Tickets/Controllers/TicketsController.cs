﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pozzani.Tickets.Data;
using Pozzani.Tickets.Models;
using System.Text.RegularExpressions;

namespace Pozzani.Tickets.Controllers
{
    public class TicketsController : Controller
    {
        private Context db = new Context();
        private AuthRepository _authRepository = null;
        private User.AccessLevelList[] _accessList = null;
        private NoteRepository noteRepository = null;
        private TicketsRepository ticketsRepository = null;
        private GoogleRepository googleRepository = null;

        public TicketsController()
        {
            _authRepository = new AuthRepository();
            noteRepository = new NoteRepository();

            _accessList = new[] {
                Models.User.AccessLevelList.IT,
                Models.User.AccessLevelList.Super,
                Models.User.AccessLevelList.Office
            };

            ViewBag.AccessList = _accessList;
        }

        // GET: Tickets
        [RedirectActionAttribute]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        // GET: Tickets/Details/5
        [RedirectActionAttribute]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);

            // Update ticket status
            ticketsRepository = new TicketsRepository();
            ticketsRepository.UpdateStatusIfNew(ticket.Id);

            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // GET: Tickets/Create
        [RedirectActionAttribute]
        public ActionResult Create()
        {
            UserRepository userRepository = new UserRepository();
            
            ViewBag.AssignedToId = new SelectList(userRepository.GetITUsers(), "Id", "Name");
            ViewBag.SubmittedById = new SelectList(userRepository.GetAllUsers(), "Id", "Name");
            ViewBag.TicketTypeId = new SelectList(db.TicketTypes, "Id", "Name");
            ViewBag.Priority = new SelectList(Enum.GetValues(typeof(Ticket.PriorityItems)));

            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RedirectActionAttribute]
        public ActionResult Create(Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                ticket.DateSubmitted = DateTime.Now;
                ticket.DateLastUpdated = DateTime.Now;
                ticket.Status = Ticket.StatusItems.New;

                // Remove scripts from html string
                Regex rRemScript = new Regex(@"<script[^>]*>[\s\S]*?</script>");
                ticket.Description = rRemScript.Replace(ticket.Description, "");

                db.Tickets.Add(ticket);
                db.SaveChanges();

                // Append to Sheet
                //googleRepository = new GoogleRepository();
                //googleRepository.AddNewRow(ticket);
                
                return RedirectToAction("Index");
            }

            ViewBag.AssignedToId = new SelectList(db.Users, "Id", "Name", ticket.AssignedToId);
            ViewBag.SubmittedById = new SelectList(db.Users, "Id", "Name", ticket.SubmittedById);
            ViewBag.TicketTypeId = new SelectList(db.TicketTypes, "Id", "Name", ticket.TicketTypeId);
            ViewBag.Priority = new SelectList(Enum.GetValues(typeof(Ticket.PriorityItems)));

            return View(ticket);
        }

        // GET: Tickets/Edit/5
        [RedirectActionAttribute]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            UserRepository userRepository = new UserRepository();

            ViewBag.AssignedToId = new SelectList(userRepository.GetITUsers(), "Id", "Name", ticket.AssignedToId);
            ViewBag.SubmittedById = new SelectList(userRepository.GetAllUsers(), "Id", "Name", ticket.SubmittedById);
            ViewBag.TicketTypeId = new SelectList(db.TicketTypes, "Id", "Name", ticket.TicketTypeId);
            ViewBag.Priority = new SelectList(Enum.GetValues(typeof(Ticket.PriorityItems)), ticket.Priority);

            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RedirectActionAttribute]
        public ActionResult Edit(Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                // Remove scripts from html string
                Regex rRemScript = new Regex(@"<script[^>]*>[\s\S]*?</script>");
                ticket.Description = rRemScript.Replace(ticket.Description, "");

                db.Entry(ticket).State = EntityState.Modified;
                db.SaveChanges();

                // Update Sheets
                //googleRepository = new GoogleRepository();
                //googleRepository.UpdateRow(ticket);

                return RedirectToAction("Index");
            }
            ViewBag.AssignedToId = new SelectList(db.Users, "Id", "Name", ticket.AssignedToId);
            ViewBag.SubmittedById = new SelectList(db.Users, "Id", "Name", ticket.SubmittedById);
            ViewBag.TicketTypeId = new SelectList(db.TicketTypes, "Id", "Name", ticket.TicketTypeId);
            ViewBag.Priority = new SelectList(Enum.GetValues(typeof(Ticket.PriorityItems)), ticket.Priority);
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        [RedirectActionAttribute]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            ticket.Status = Ticket.StatusItems.Deleted;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [RedirectActionAttribute]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            db.Tickets.Remove(ticket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult AssignToMe(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Where(x => x.Id == id).SingleOrDefault();
            ticket.AssignedToId = AuthRepository.GetUser().Id;
            ticket.Status = Ticket.StatusItems.Assigned;
            db.SaveChanges();

            if (ticket == null)
            {
                return HttpNotFound();
            }

            // Add a note to the Ticket to say it's been assigned to
            noteRepository.AddAssignedNote(ticket.Id, AuthRepository.GetUser());

            TempData["success"] = "This ticket is now assigned to you.";
            return View("Details", ticket);
        }

        public ActionResult Complete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Where(x => x.Id == id).SingleOrDefault();
            ticket.Status = Ticket.StatusItems.Completed;
            db.SaveChanges();

            if (ticket == null)
            {
                return HttpNotFound();
            }

            // Add a note to the Ticket to say it's been assigned to
            noteRepository.CompletedNote(ticket.Id, AuthRepository.GetUser());

            TempData["success"] = "This ticket is now completed.";
            return View("Details", ticket);
        }
    }
}
