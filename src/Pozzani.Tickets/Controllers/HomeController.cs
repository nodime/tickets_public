﻿using Pozzani.Tickets.Data;
using Pozzani.Tickets.Models;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web.Mvc;

namespace Pozzani.Tickets.Controllers
{
    public class HomeController : Controller
    {
        private AuthRepository _authRepository = null;
        private User.AccessLevelList[] _accessList = null;
        readonly string startingCell = "A3";

        public HomeController()
        {
            _authRepository = new AuthRepository();

            _accessList = new [] {
                Models.User.AccessLevelList.IT,
                Models.User.AccessLevelList.Office,
                Models.User.AccessLevelList.Super
            };

            ViewBag.AccessList = _accessList;
        }

        // GET: Home
        [RedirectActionAttribute]
        public ActionResult Index()
        {
            var ticketRepository = new TicketsRepository();
            var tickets = ticketRepository.GetAllActiveTickets().OrderBy(x => x.PriorityLevel).OrderByDescending(x => x.Priority).ToList();
            tickets.RemoveAll(x => x.TicketTypeId == 2 || x.TicketTypeId == 3);
            return View(tickets);
        }

        /// <summary>
        /// Feature list for Tickets
        /// </summary>
        /// <returns></returns>
        [RedirectActionAttribute]
        public ActionResult FeatureList()
        {
            var ticketRepository = new TicketsRepository();
            var tickets = ticketRepository.GetAllActiveTickets();
            
            return View(tickets.FindAll(x => x.TicketTypeId == 2 || x.TicketTypeId == 3));
        }

        /// <summary>
        /// Non authorised users will end up here
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexRestricted()
        {
            return View();
        }

        /// <summary>
        /// Sheets intergration
        /// </summary>
        /// <returns></returns>
        public ActionResult Sheets()
        {
            GoogleRepository googleRepository = new GoogleRepository();
            TicketsRepository ticketRepository = new TicketsRepository();

            

            //foreach (Ticket ticket in ticketRepository.GetAllActiveTickets())
            //{
            //    //googleRepository.AddNewRow(ticket);
            //    //googleRepository.UpdateRow(ticket);
            //    break;
            //}

            //ViewBag.Rows = googleRepository.GetSheetsValues("A3:F500");

           

            return View();
        }
    }
}