﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pozzani.Tickets.Data;
using Pozzani.Tickets.Models;
using System.Text.RegularExpressions;

namespace Pozzani.Tickets.Controllers
{
    public class NotesController : Controller
    {
        private Context db = new Context();
        private AuthRepository _authRepository = null;
        private User.AccessLevelList[] _accessList = null;

        public NotesController()
        {
            _authRepository = new AuthRepository();

            _accessList = new[] {
                Models.User.AccessLevelList.IT,
                Models.User.AccessLevelList.Super
            };

            ViewBag.AccessList = _accessList;
        }

        // GET: Notes/Create
        public ActionResult Create()
        {
            ViewBag.TicketID = new SelectList(db.Tickets, "Id", "Description");
            ViewBag.SubmittedByID = new SelectList(db.Users, "Id", "Username");
            return View();
        }

        // POST: Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Note note)
        {
            if (ModelState.IsValid)
            {
                note.DateSubmitted = DateTime.Now;
                note.SubmittedByID = AuthRepository.GetUser().Id;

                // Remove scripts from html string
                Regex rRemScript = new Regex(@"<script[^>]*>[\s\S]*?</script>");
                note.Description = rRemScript.Replace(note.Description, "");

                db.Notes.Add(note);
                db.SaveChanges();
                return RedirectToAction("Details", "Tickets", new { Id = note.TicketID });
            }

            TempData["error"] = "Please enter a note before clicking the button!";
            return RedirectToAction("Details", "Tickets", new { Id = note.TicketID });
        }

       // GET: Notes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Note note = db.Notes.Find(id);
            db.Notes.Remove(note);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
