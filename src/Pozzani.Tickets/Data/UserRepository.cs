﻿using Pozzani.Tickets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pozzani.Tickets.Data
{
    public class UserRepository
    {
        Context db = new Context();

        /// <summary>
        /// Returns a complete list of users
        /// </summary>
        /// <returns></returns>
        public List<User> GetAllUsers()
        {
            return db.Users.ToList();
        }

        /// <summary>
        /// Get the user based on their Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUserById(int? id)
        {
            if (id == null)
            {
                return new User { Id = 0, Name = "N/A" };
            } else
            {
                return db.Users.Where(x => x.Id == id).SingleOrDefault();
            }
        }
        
        /// <summary>
        /// Get all IT users
        /// </summary>
        /// <returns></returns>
        public List<User> GetITUsers()
        {
            return db.Users.Where(u => u.AccessLevel == User.AccessLevelList.IT || u.AccessLevel == User.AccessLevelList.Super).ToList();
        }
    }
}