﻿using Pozzani.Tickets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pozzani.Tickets.Data
{
    public class RedirectActionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            User.AccessLevelList[] accessList = filterContext.Controller.ViewBag.AccessList;

            // If user is not on the list s/he don't get in!
            if (! AuthRepository.IsAuth(accessList) || accessList == null)
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { Controller = "Home", Action = "IndexRestricted" }));
            }
        }
    }
}