﻿using Pozzani.Tickets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pozzani.Tickets.Data
{
    public class TicketsRepository
    {
        Context db = null;

        public TicketsRepository()
        {
            db = new Context();
        }
        /// <summary>
        /// Gets a full list of tickets
        /// </summary>
        /// <returns></returns>
        public List<Ticket> GetAllActiveTickets()
        {
            var result = db.Tickets.ToList();
            result.RemoveAll(x => x.Status == Ticket.StatusItems.Deleted);
            result.RemoveAll(x => x.Status == Ticket.StatusItems.Completed);
            result.RemoveAll(x => x.Status == Ticket.StatusItems.Paused);
            return result;
            //return ctx.Tickets.Where(t => t.Status != Ticket.StatusItems.Completed || t.Status != Ticket.StatusItems.Deleted || t.Status != Ticket.StatusItems.Paused).ToList();
        }

        /// <summary>
        /// Will check to see whether status is new for selected ticket then proceed to update it to either assigned or unassigned
        /// </summary>
        /// <param name="ticketId"></param>
        public void UpdateStatusIfNew(int ticketId)
        {
            if (IsNew(ticketId))
            {
                var result = db.Tickets.Where(x => x.Id == ticketId).SingleOrDefault();
                result.Status = (result.AssignedToId == null ? Ticket.StatusItems.UnAssigned : Ticket.StatusItems.Assigned);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Returns whether ticket status is new
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        private bool IsNew(int ticketId)
        {
            var result = db.Tickets.Where(x => x.Id == ticketId && x.Status == Ticket.StatusItems.New).FirstOrDefault();
            return (result == null ? false : true);
        }
    }
}