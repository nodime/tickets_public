﻿using System;

namespace Pozzani.Tickets.Model
{

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CascadeDeleteAttribute : Attribute { }
}
