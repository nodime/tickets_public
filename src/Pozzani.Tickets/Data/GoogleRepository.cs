﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Google.Apis.Sheets.v4.SheetsService;
using Pozzani.Tickets.Models;
using System.Collections;
using System;

namespace Pozzani.Tickets.Data
{
    public class GoogleRepository
    {
        static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static readonly string ApplicationName = "Pozzani Tickets";
        static readonly string SpreadsheetId = "1WrMujRspLWEvFdvrwYGutZ2gte-nY92pam5hrLI598s";
        //static readonly string Range = "Overview!A2:A100";
        static SheetsService service;

        UserCredential googleCredential;

        public GoogleRepository()
        {
            // Get me some google creds
            googleCredential = InitGoogleCredentials();

            // Create me a google sheet api service
            service = InitGoogleSheetsService();
        }

        /// <summary>
        /// Instantiate a new Google Credentials that uses the json data
        /// </summary>
        /// <returns></returns>
        private UserCredential InitGoogleCredentials()
        {
            //return UserCredential.FromStream(new FileStream("client_secret.json", FileMode.Open, FileAccess.Read)).CreateScoped(Scopes);
            var stream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read);
            string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-pozzani-tickets.json");

            return GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets, Scopes, "user", CancellationToken.None, new FileDataStore(credPath, true)).Result;
        }

        /// <summary>
        /// Instantiate the Google sheets service
        /// </summary>
        /// <returns></returns>
        private SheetsService InitGoogleSheetsService()
        {
            return new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = googleCredential,
                ApplicationName = ApplicationName,
            });
        }

        /// <summary>
        /// Gets data from cells within a range
        /// </summary>
        /// <param name="Range"></param>
        /// <returns></returns>
        public List<Ticket> GetSheetsValues(string Range)
        {
            List<Ticket> tickets = new List<Ticket>();
            SpreadsheetsResource.ValuesResource.GetRequest request = service.Spreadsheets.Values.Get(SpreadsheetId, Range);
            ValueRange response = request.Execute();

            IList<IList<object>> values = response.Values;

            if (values != null && values.Count > 0)
            {
                
                foreach (var row in values)
                {
                    Ticket ticket = new Ticket();

                    ticket.Id = int.Parse(row[0].ToString());
                    ticket.AssignedTo = new User { Name = row[1].ToString() };
                    ticket.SubmittedBy = new User { Name = row[2].ToString() };
                    ticket.Description = row[3].ToString();
                    ticket.Status = getStatusValue(row[4].ToString());

                    tickets.Add(ticket);
                }
            }

            return tickets;
        }
        
        /// <summary>
        /// This will take a ticket and add it to the Overview sheet and append it to the bottom.
        /// </summary>
        /// <param name="ticket"></param>
        public void AddNewRow(Ticket ticket)
        {
            SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum valueInputOption =  SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum insertDataOption =  SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            string range = "Overview!A3";
            ValueRange requestBody = new ValueRange();
            var row = new string[] {
                ticket.Id.ToString(),
                (ticket.AssignedTo == null ? "-" : ticket.AssignedTo.Name),
                ticket.SubmittedBy.Name,
                ticket.Description,
                ticket.Status.ToString()
            };
            requestBody.Values = new List<IList<object>> { row };

            SpreadsheetsResource.ValuesResource.AppendRequest request = service.Spreadsheets.Values.Append(requestBody, SpreadsheetId, range);
            request.ValueInputOption = valueInputOption;
            request.InsertDataOption = insertDataOption;

            AppendValuesResponse response = request.Execute();
        }

        public void UpdateRow(Ticket ticket)
        {
            int ticketNumber = 3;
            SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum valueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
            // 1. Loop through all tickets
            foreach (Ticket item in GetSheetsValues("A3:E500"))
            {
                if (item.Id == ticket.Id)
                {
                    // Found it lets stop here and move on.

                    break;
                }

                // increament by 1 each time
                ticketNumber++;
            }

            // Row to update
            string range = "Overview!A" + ticketNumber;

            ValueRange requestBody = new ValueRange();
            var row = new string[] {
                    ticket.Id.ToString(),
                    (ticket.AssignedTo == null ? "-" : ticket.AssignedTo.Name),
                    ticket.SubmittedBy.Name,
                    ticket.Description,
                    ticket.Status.ToString()
                };
            requestBody.Values = new List<IList<object>> { row };

            SpreadsheetsResource.ValuesResource.UpdateRequest request = service.Spreadsheets.Values.Update(requestBody, SpreadsheetId, range);
            request.ValueInputOption = valueInputOption;

            UpdateValuesResponse response = request.Execute();
        }

        #region helpers
        private Ticket.StatusItems getStatusValue(string val)
            {
                switch (val)
                {
                    case "New":
                        return Ticket.StatusItems.New;
                    case "UnAssigned":
                        return Ticket.StatusItems.UnAssigned;
                    case "Assigned":
                        return Ticket.StatusItems.Assigned;
                    case "Paused":
                        return Ticket.StatusItems.Paused;
                    case "Awaiting_Feedback":
                        return Ticket.StatusItems.Awaiting_Feedback;
                    case "Completed":
                        return Ticket.StatusItems.Completed;
                    case "Deleted":
                        return Ticket.StatusItems.Deleted;
                }

                return Ticket.StatusItems.New;
            }
        #endregion
    }
}