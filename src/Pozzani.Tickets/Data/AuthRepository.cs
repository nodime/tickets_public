﻿using Pozzani.Tickets.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pozzani.Tickets.Data
{
    public class AuthRepository
    {
        private string _user = null;

        public static User.AccessLevelList AccessLevel = User.AccessLevelList.Disabled;

        public AuthRepository()
        {
            // Get the current user
            _user = HttpContext.Current.User.Identity.Name;

            // Do they exist in the database?
            DoesUserExist(_user);

            // TODO: Add some sort of user access?
            AccessLevel = GetUserAccessLevel();
        }

        /// <summary>
        /// Check to see whether user existing in the db
        /// </summary>
        /// <param name="user"></param>
        public void DoesUserExist(string user)
        {
            Context ctx = new Context();
            if (ctx.Users.Where(u => u.Username == user).Count() > 0)
            {
                // User exists
            } else
            {
                AddUser(user);
            }
        }

        /// <summary>
        /// Add or Update user in database
        /// </summary>
        /// <param name="user"></param>
        public void AddUser(string user)
        {
            Context ctx = new Context();
            ctx.Users.AddOrUpdate(
                u => u.Username,
                new User {
                    Username = user,
                    Email = "",
                    Name = user.Substring(user.IndexOf("\\") + 1),
                    AccessLevel = User.AccessLevelList.Disabled
                }
            );

            ctx.SaveChanges();
        }

        /// <summary>
        /// Find a user in the database and return their access level
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private User.AccessLevelList GetUserAccessLevel()
        {
            Context ctx = new Context();
            var foundUser = ctx.Users.Where(u => u.Username == _user).FirstOrDefault();

            // If user is found return the users access level, otherwise return 0
            if (foundUser.AccessLevel !=  User.AccessLevelList.Disabled)
            {
                return foundUser.AccessLevel;
            } else
            {
                return User.AccessLevelList.Disabled;
            }
        }

        /// <summary>
        /// Check if current user is allowed to view page that supplied list
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static bool IsAuth(User.AccessLevelList[] list)
        {
            if (list == null)
            {
                return false;
            }

            if (list.Contains(AccessLevel))
            {
                return true;
            }
            return false;
        }

        public static User GetUser()
        {
            Context ctx = new Context();
            AuthRepository authRepository = new AuthRepository();
            return ctx.Users.Where(u => u.Username == authRepository._user).SingleOrDefault();
        }
    }
}