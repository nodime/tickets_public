﻿using Pozzani.Tickets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Pozzani.Tickets.Data
{
    public class NoteRepository
    {
        Context db = null;

        public NoteRepository()
        {
            db = new Context();
        }

        public void AddAssignedNote(int ticketId, User user) 
        {
            Note note = new Note()
            {
                Description = "Ticket has been assigned to " + user.Name,
                Minutes = 0,
                SubmittedByID = user.Id,
                TicketID = ticketId,
                DateSubmitted = DateTime.Now
            };

            db.Notes.Add(note);
            db.SaveChanges();
        }

        public void CompletedNote(int ticketId, User user)
        {
            Note note = new Note()
            {
                Description = "Ticket has been completed",
                Minutes = 0,
                SubmittedByID = user.Id,
                TicketID = ticketId,
                DateSubmitted = DateTime.Now
            };

            db.Notes.Add(note);
            db.SaveChanges();
        }
    }
}