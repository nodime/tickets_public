namespace Pozzani.Tickets.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingNotes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Notes", "Description", c => c.String(nullable: false, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Notes", "Description", c => c.String(unicode: false));
        }
    }
}
