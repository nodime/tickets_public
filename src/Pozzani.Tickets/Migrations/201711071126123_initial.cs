namespace Pozzani.Tickets.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(unicode: false),
                        Minutes = c.Int(nullable: false),
                        DateSubmitted = c.DateTime(nullable: false, precision: 0),
                        SubmittedByID = c.Int(nullable: false),
                        TicketID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tickets", t => t.TicketID)
                .ForeignKey("dbo.Users", t => t.SubmittedByID)
                .Index(t => t.SubmittedByID)
                .Index(t => t.TicketID);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, unicode: false),
                        DateSubmitted = c.DateTime(nullable: false, precision: 0),
                        DateLastUpdated = c.DateTime(nullable: false, precision: 0),
                        DateCompleted = c.DateTime(precision: 0),
                        AssignedToId = c.Int(),
                        SubmittedById = c.Int(nullable: false),
                        TicketTypeId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AssignedToId)
                .ForeignKey("dbo.Users", t => t.SubmittedById)
                .ForeignKey("dbo.TicketTypes", t => t.TicketTypeId)
                .Index(t => t.AssignedToId)
                .Index(t => t.SubmittedById)
                .Index(t => t.TicketTypeId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(unicode: false),
                        Name = c.String(unicode: false),
                        Email = c.String(unicode: false),
                        AccessLevel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notes", "SubmittedByID", "dbo.Users");
            DropForeignKey("dbo.Notes", "TicketID", "dbo.Tickets");
            DropForeignKey("dbo.Tickets", "TicketTypeId", "dbo.TicketTypes");
            DropForeignKey("dbo.Tickets", "SubmittedById", "dbo.Users");
            DropForeignKey("dbo.Tickets", "AssignedToId", "dbo.Users");
            DropIndex("dbo.Tickets", new[] { "TicketTypeId" });
            DropIndex("dbo.Tickets", new[] { "SubmittedById" });
            DropIndex("dbo.Tickets", new[] { "AssignedToId" });
            DropIndex("dbo.Notes", new[] { "TicketID" });
            DropIndex("dbo.Notes", new[] { "SubmittedByID" });
            DropTable("dbo.TicketTypes");
            DropTable("dbo.Users");
            DropTable("dbo.Tickets");
            DropTable("dbo.Notes");
        }
    }
}
