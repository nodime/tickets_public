namespace Pozzani.Tickets.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPriorityLevelPropertyToTicket : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "PriorityLevel", c => c.Int(nullable: false, defaultValueSql: "5"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "PriorityLevel");
        }
    }
}
