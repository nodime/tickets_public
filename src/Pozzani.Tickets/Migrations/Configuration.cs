namespace Pozzani.Tickets.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Pozzani.Tickets.Data.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
        }

        protected override void Seed(Pozzani.Tickets.Data.Context context)
        {
            context.TicketTypes.AddOrUpdate(
              t => t.Name,
              new Models.TicketType { Id = 1, Name = "Generic" }
            );
        }
    }
}
